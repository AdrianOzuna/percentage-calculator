puts "What price did you set for your gig?"
price = gets.to_f

#Get percentage, will output the real amount of money the seller will get
after_fee = price * 20 / 100
total = price - after_fee

puts "#{after_fee} dollars will be discounted after fee. You will receive a total of #{total} dollars"
